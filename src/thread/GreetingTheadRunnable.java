package thread;

public class GreetingTheadRunnable {
	
	public static void main(String[] args) throws InterruptedException{
		
		BankAccount acc = new BankAccount(0);
		Runnable dep = new threadLecture("Deposit",acc);
		Runnable with = new threadLecture("Withdraw",acc);
		
		Thread t1 = new Thread(dep);
		Thread t2 = new Thread(with);
		
		t1.start();
		t2.start();
		
		
	}

}
