package thread;

import java.util.Date;

public class threadLecture implements Runnable{
	
	private static final int REPETITIONS = 10;
	private static final int DELAY = 0;
	
	private String greeting;
	private BankAccount bank;
	
	public threadLecture(String aGreeting,BankAccount bank){
		greeting = aGreeting;
		this.bank = bank;
	}


	@Override
	public void run() {
		try
		{
			for (int i = 1; i <= REPETITIONS; i++){
				
				Date now = new Date();
				
				if(greeting.equalsIgnoreCase("Deposit")){
					bank.deposit(100);
					System.out.println(now + " " + greeting+" 100");
				}
				else{
					bank.withDraw(100);
					System.out.println(now + " " + greeting+" 100");
					}
				System.out.println("BALANCE: "+bank.getBalance());
				Thread.sleep(DELAY);
			}
		}
		catch (InterruptedException exception){
			System.err.println("Interrupted");
		}
	}
}
